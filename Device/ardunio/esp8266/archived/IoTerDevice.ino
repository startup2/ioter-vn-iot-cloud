#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecure.h>
#include "Data.h"

const char* ssid     = "ssid";
const char* password = "password";
const char* streamId   = "";
const char* deviceID = "";
const char* token = "";

const char* host = "http://ioter-device.avnhome.com/jsond";

Data data;

void setup() {
  Serial.begin(115200);
  randomSeed(analogRead(0));
  delay(10);
  data = Data();

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

int value = 0;

void loop() {
  delay(5000);

  //Prepare Data
  data.getData();
  String output = data.toString();
  Serial.println(output);
  String postData = data.toJson<200>();
  Serial.println(postData);
  delay(1000);  

  // We now create a URI for the request  
  String url = "?k=";
  url += streamId;
  url += "&i=";
  url += deviceID;
  Serial.print("Send data to ");
  String address = host + url;
  Serial.println(address);

  //Prepare Client
  HTTPClient http;
  http.begin(address);  
  http.addHeader("Content-Type", "application/json");
  http.addHeader("X-Auth-Token", token);

  //Send to clound
  int httpCode = http.POST(postData);

  //Waiting to reponse
  Serial.println(httpCode);
  Serial.println(http.getString());

  //Close client
  http.end();

  Serial.println("------------");
  Serial.println("closing connection");
}