#include "Data.h"

String Data::toString(){
    String builder = "";
    builder += "Data(";
    builder += "property1: ";
    builder += String(property1) + ", ";
    builder += "property2: ";
    builder += String(property2) + ", ";
    builder += "property3: ";
    builder += String(property3) + ", ";
    builder += "property4: ";
    builder += String(property4) + ", ";
    builder += "property5: ";
    builder += String(property5) + ", ";
    builder += "property6: ";
    builder += String(property6) + ", ";
    builder += "property7: ";
    builder += String(property7) + ", ";
    builder += "property8: ";
    builder += String(property8) + ")";
    return builder;
};

template<size_t N>
String Data::toJson(){
    StaticJsonDocument<N> doc;
    doc["p1"] = String(property1);
    doc["p2"] = String(property2);
    doc["p3"] = String(property3);
    doc["p4"] = String(property4);
    doc["p5"] = String(property5);
    doc["p6"] = String(property6);
    doc["p7"] = String(property7);
    doc["p8"] = String(property8);
    String output;
    serializeJson(doc, output);
    return output;
};

void Data::getData(){
    property1 = random(0,100);
    property2 = random(0,100);
    property3 = random(0,100);
    property4 = random(0,100);
    property5 = random(0,100);
    property6 = random(0,100);
    property7 = random(0,100);
    property8 = random(0,100);
};