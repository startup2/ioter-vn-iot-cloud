
**README**

VN IoT Cloud – Ioter – Device/Arduino
APP hiện không hỗ trợ code này nữa. Hãy quay lại!.
**ESP8266**

1.  **Hướng dẫn tạo môi trường cho ESP8266**

    Làm theo link dưới đây:

    <http://arduino.vn/bai-viet/1172-lap-trinh-esp8266-bang-arduino-ide>

2.  **Hướng dẫn cài đặt thư viện**

    Vào Tool -\> Library Manager, hộp thoại xuất hiện.

    Search ArduinoJson, chon version 6.10.1, nhấn Install

3.  **Hướng dẫn cài đặt kết nối**

    Kết nối module vào máy tính, và chọn cổng COM tương ứng như sau:

    Vào Tool -\> Board -\> Port và chon COM

4.  **Hướng dẫn code sample**

    Mở IoterDevice.ino trong thư mục esp8266 và thiết lập các tham số sau:

    const char\* ssid     = "ssid";

    const char\* password = "password";

    const char\* streamId   = "";

    const char\* deviceID = "";

    const char\* token = "";

-   ssid: Tên đăng nhập wifi

-   password: Mật khẩu wifi

-   streamId: mã stream Id được lấy từ app IoTer với thuộc tính k

-   deviceID: ID của device được lấy từ app IoTer

-   token: token được lấy từ app Ioter

    Link tải app Ioter:

    <https://play.google.com/store/apps/details?id=com.avnhome.ioter>

1.  **Hướng dẫn nạp code và xem kết quả:**

    Nhấn tổ hợp phím Ctrl + U để nạp code cho device

    Chon Tool -\> Serial Monitor để xem kết quả
