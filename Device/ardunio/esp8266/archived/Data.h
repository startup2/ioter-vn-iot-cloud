#include <ArduinoJson.h>
#include <Arduino.h>
#pragma once

class Data {
    private:                
        int property1;
        int property2;
        int property3;
        int property4;
        int property5;
        int property6;
        int property7;
        int property8;

    public:
        String toString();
        template<size_t N>
        String toJson();
        void getData();

};