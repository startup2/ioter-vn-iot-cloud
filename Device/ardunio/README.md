# README #

VN IoT Cloud - Ioter - Device/Arduino

Lưu ý: từ version 3.1.1 app đã hổ trợ thiết bị MQTT.

### Cách gửi dữ liệu lên IoTer dùng mqtt   ###

Hostname: ioter-device.avnhome.com
PORT: 1883
Protocol: MQTT

MQTT Topic: /59ea69d4-7768-4cb8-9147-79613c70ebc0/<Id Thiết bị>/attrs
ví dụ: /59ea69d4-7768-4cb8-9147-79613c70ebc0/7bddb05e-2977-47d5-bcd3-271e62d07492/attrs

Body: p1|<Property1 Valute>|p2|<Property2 Valute>|p3|<Property3 Valute>|p4|<Property4 Valute>|p5|<Property5 Valute>|p6|<Property6 Valute>|p7|<Property7 Valute>|p8|<Property8 Valute>
ví dụ: p1|1|p2|2|p3|3|p4|4|p5|5|p6|6|p7|7|p8|8



### Cách Điều khiển thiết bị từ app dùng mqtt   ###
Hostname: ioter-device.avnhome.com
PORT: 1883
Protocol: MQTT
Method: Cần đăng kí vô mqtt topic như bên dưới.
MQTT Topic: /59ea69d4-7768-4cb8-9147-79613c70ebc0/<Id Thiết bị>/cmd
ví dụ: /59ea69d4-7768-4cb8-9147-79613c70ebc0/7bddb05e-2977-47d5-bcd3-271e62d07492/cmd

Body sẽ nhận về từ ioter như sau: : <Id Thiết bị>@<Tên command>|
ví dụ: 7bddb05e-2977-47d5-bcd3-271e62d07492@command1
Dựa theo command có thể implement hàm tương ứng để thụ lý command.

### MQTT cho testing miễn phí tại: 

Hostname: ioter-device.avnhome.com
PORT: 1883
Protocol: MQTT

Free usage for any manual test.

### Lưu ý
Code mẫu cho ardunio sẽ được cập nhật sớm.
youtube video về ví dự mẫu sẽ cập nhật sớm tại kênh này: https://www.youtube.com/channel/UCAHxSBW_4wz8fSXf4bB8VUw

### Hướng dẫn đóng góp ###
IoTer rất hoan nghiên sự đóng góp của mọi người cho IoTer để biến IoTer thành No.1 Iot Cloud miễn phí.
 
* Mọi người có thể chia sẽ mã nguồn, thư viện mình viết, website, ứng dụng di động hay bất bì liên quan đến ioter.


### Cần hổ trợ! ###
Các bạn liên hệ qua thông tin bên dưới nhé. Happy coding, happy learning!
* email: avehouse8@gmail.com
* facebook page: https://www.facebook.com/vniotcloud
* trang chủ: https://ioter.avnhome.com

### Donate
Hệ thống cần sự hổ trợ từ bạn để duy trì và phát triển hơn nữa.
Do đó đừng ngần ngại hổ trợ hệ thống nhé.
Buy "Hệ thống" a day life! qua các kênh sau:

Momo, Viettel pay: 0973906776.
Paypal: https://www.paypal.me/avehouse8



