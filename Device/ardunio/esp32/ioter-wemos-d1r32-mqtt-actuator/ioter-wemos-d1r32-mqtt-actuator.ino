/*
*/


#include <WiFi.h>
#include <PubSubClient.h>

const char* mqtt_server = "ioter-device.avnhome.com";
const char* topic_prefix="/59ea69d4-7768-4cb8-9147-79613c70ebc0/";
const char* sub_topic_suffix = "/cmd";
const char* pub_topic_suffix="/attrs";

/*
Nhập tên wifi va mat khau.
Id_Thiet_Bi: Lay tu app.
*/
const char* ssid = "TEN-WIFI";
const char* password = "MAT-KHAU-WIFI";
const char* Id_Thiet_Bi="Id-Thiet-Bi-Lay-Tu-App";

WiFiClient espClient;
PubSubClient client(espClient);


void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++)
  {
    Serial.println((char)payload[i]);
  }
  if ((char)payload[length-2] == '1') //command1
  {
 
    command1(topic, payload, length);
 
  }
  else if ((char)payload[length-2] == '2') //command2
  {
    command2(topic, payload, length); 
  }
  else if ((char)payload[length-2] == '3') //command3
  {
    command3(topic, payload, length);  
  }
    else if ((char)payload[length-2] == '4') //command4
  {
    command4(topic, payload, length);  
  }
  else{
    Serial.println("Invalid command");
  }
}

/*
Ham nay giup ket noi thiet bi len IoTer - VN IoT Cloud.
*/
void connect2IoTer(){
String sub_topic="";
  sub_topic.concat(topic_prefix);
  sub_topic.concat(Id_Thiet_Bi);
  sub_topic.concat(sub_topic_suffix);
  Serial.println("sub_topic");
  Serial.println(sub_topic);
  client.setServer(mqtt_server, 1883);//connecting to mqtt server
  client.setCallback(callback);

    Serial.println("Dang ket noi toi IoTer ...");
    if (client.connect(Id_Thiet_Bi)) {
      Serial.println("connected");
      client.subscribe(sub_topic.c_str());

    } else {
      Serial.print("Khong the ket noi toi IoTer");
      Serial.print(client.state());
      Serial.println("Thu lai sau 5 giay");
      delay(5000);
    }
    
  
}
/*
Ham nay giup gui du lieu tu "body" len IoTer - VN IoT Cloud. 
*/

void publishData2IoTer(char* body){
String pub_topic="";
  pub_topic.concat(topic_prefix);
  pub_topic.concat(Id_Thiet_Bi);
  pub_topic.concat(pub_topic_suffix);
  if (!client.connected()) {
    connect2IoTer();
  }
 client.publish(pub_topic.c_str(), body);
}

/*Tu hien thuc command 1*/
void command1(char* topic, byte* payload, unsigned int length){
  Serial.println("command1");
}

/*Tu hien thuc command 2*/
void command2(char* topic, byte* payload, unsigned int length){
  Serial.println("command2");
}
/*Tu hien thuc command 3*/
void command3(char* topic, byte* payload, unsigned int length){
  Serial.println("command3");
}
/*Tu hien thuc command 4*/
void command4(char* topic, byte* payload, unsigned int length){
  Serial.println("command4");
}

void reconnect() {
  while (!client.connected()) {
    connect2IoTer();
  }
}
void setup()
{
  Serial.begin(115200);
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Connecting to MQTT Server");
  connect2IoTer();

}


void loop()
{
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  delay(5000);
  //gui du lieu len IoTer - VN IoT Cloud
  char* body = "p1|191|p2|72|p3|73|p4|74|p5|75|p6|76|p7|77|p8|78";
  publishData2IoTer(body);
  
  
}
