# README #

Device\ardunio\

Code mẫu cho esp32 gửi dữ liệu và nhận lệnh từ IoTer - VN IoT Cloud


### Hướng dẫn đóng góp ###
* IoTer rất hoan nghiên sự đóng góp của mọi người cho IoTer để biến IoTer thành No.1 Iot Cloud miễn phí.
 
* Mọi người có thể chia sẽ mã nguồn, thư viện mình viết, website, ứng dụng di động hay bất bì liên quan đến ioter.


### Cần hổ trợ! ###
* Các bạn liên hệ qua thông tin bên dưới nhé. Happy coding, happy learning!
* email: avehouse8@gmail.com
* facebook page: https://www.facebook.com/vniotcloud
* trang chủ: https://ioter.avnhome.com

### Donate
* Hệ thống cần sự hổ trợ từ bạn để duy trì và phát triển hơn nữa.
* Do đó đừng ngần ngại hổ trợ hệ thống nhé.
* Buy "Hệ thống" a day life! qua các kênh sau:

* Momo, Viettel pay: 0973906776.
* Paypal: https://www.paypal.me/avehouse8

