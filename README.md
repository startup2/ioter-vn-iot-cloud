# README #

VN IoT Cloud - Ioter - 3.1.1
![ioter-flow](ioter-flow.jpg)

### Thông tin về Repository   ###

* Repository chứa các thông tin và hướng dẫn sử dụng ioter của ioter dev team và cộng đồng hỗ trợ.
* 3.1.1 phiên bản này sẽ được cập nhật theo phiên bản của ứng dụng ioter.
* Các hướng dẫn từng từng bước từng bước sẽ có trong từng thư mục

### Cấu trúc thư mục? ###

* API:  chứa thông tin về các API mở được cung cấp bởi IoTer kèm theo hướng dẫn sử dụng và các lưu ý nếu có.
* App: Chứa thông tin hướng dẫn và các lưu ý nếu có trong quá trình phát triển ứng dụng di động android, ios.
* Web: Chứa thông tin hướng dẫn và các lưu ý nếu có trong quá trình phát triển ứng dụng web.
* Device: Chứa thông tin hướng dẫn và cách kết nối thiết bị với IoTer bao gồm thư viện và code mẫu.
* Other: Chứa các thông tin liên quan khác.
* Sample: chứa thông tin được chia sẽ từ cộng đồng.


### Hướng dẫn đóng góp ###
* IoTer rất hoan nghiên sự đóng góp của mọi người cho IoTer để biến IoTer thành No.1 Iot Cloud miễn phí.
 
* Mọi người có thể chia sẽ mã nguồn, thư viện mình viết, website, ứng dụng di động hay bất bì liên quan đến ioter.


### Cần hổ trợ! ###
* Các bạn liên hệ qua thông tin bên dưới nhé. Happy coding, happy learning!
* email: avehouse8@gmail.com
* facebook page: https://www.facebook.com/vniotcloud
* trang chủ: https://ioter.avnhome.com

### Donate
* Hệ thống cần sự hổ trợ từ bạn để duy trì và phát triển hơn nữa.
* Do đó đừng ngần ngại hổ trợ hệ thống nhé.
* Buy "Hệ thống" a day life! qua các kênh sau:

* Momo, Viettel pay: 0973906776.
* Paypal: https://www.paypal.me/avehouse8


